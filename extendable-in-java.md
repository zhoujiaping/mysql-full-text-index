# java中的可拓展性

## 什么是可拓展性
简单理解就是添加新功能的容易程度。

## 可拓展性分类
* 功能拓展
* 性能拓展

  本篇只讨论功能拓展

## 功能拓展
1. 静态拓展

    拓展要生效，必须重新编译代码。
2. 动态拓展

    不需要重新编译代码，可以在运行时加入新的代码并执行。

静态拓展 vs 动态拓展

静态拓展需要修改源码。我们说它可拓展性不好。

因为我们要获取源码，修改并编译。

如果你尝试过编译spring源码，你会发现这很难。

动态拓展，只需要编译拓展的那部分代码，原来的代码不需要重新编译。



## 讨论可拓展性的时候，我们先分析一下开发者的角色。
1. 库提供者
2. 库拓展者
3. 库的用户

思考：为什么我们学这么多设计模式，却发现用的很少，而框架中却到处都是？

思考：什么时候需要可拓展性？


## 可拓展性实现思路
基本思想，运行时绑定，运行时选择要执行的代码，而且这部分代码可以运行时添加。


## 具体实现方式
1. if-else/switch

   需要添加新功能的时候，修改原来的代码，增加新的分支。

   运行时根据条件执行分支。不支持运行时添加分支。
2. 多态

   各种设计模式。包括创建型、结构型、行为型。

   运行时根据类型分派到不同的方法实现。

   （基于java多态实现的动态拓展是一维的拓展，并不支持多维。

   java的动态分派是单分派的，不过java可以通过命令模式实现双分派。
    
   至于多分派，一般不考虑这个问题。）

### 复习一下设计模式
* 创建型模式

    单例模式、抽象工厂模式、建造者模式、工厂模式、原型模式。
* 结构型模式

    适配器模式、桥接模式、装饰模式、组合模式、外观模式、享元模式、代理模式。
   
* 行为型模式

    模版方法模式、命令模式、迭代器模式、观察者模式、中介者模式、备忘录模式、解释器模式、状态模式、策略模式、职责链模式(责任链模式)、访问者模式。


    比如策略模式，常用于动态选择不同的算法。
	需要支持新的算法实现的时候，只需要添加一个算法的实现类。

	比如工厂模式，根据算法名称，创建不同的算法实现类。
	java中的消息摘要算法，有md5，sha128，sha256等各种实现。

	比如模板方法模式，父类实现基本流程，不同子类提供不同的方法实现。


3. 查表法

    可以是数组，下标对应入参，数组元素对应要执行的代码地址。

    可以是哈希表，key为场景标识，value为处理函数。

    对于spring应用，由于spring上下文是一个对象容器，key为bean类型或者beanName，value为bean。

    所以可以用查表法来实现拓展性。
    
    动态参数为beanName，然后根据beanName找到bean，然后执行bean的方法。

	这样就实现了根据动态参数选择了要执行的代码，当然只能在几个方法之间选择执行哪一个。


思考：如何衡量一段代码/一个设计的可拓展性？

耦合以及约束越弱，可拓展性越强

例0：
```
/**接口对入参和出参的约束较小，并不限定为字符串，不仅仅可以用于字符串的加解密，还可以拓展为图片、pdf文件等加解密*/
interface Crypter{
	byte[] encrypt(byte[] data,byte[] key);
	byte[] decrypt(byte[] data,byte[] key);
}
class RSACrypter implements Crypter{
	byte[] encrypt(byte[] data,byte[] key){
		...
	}
	byte[] decrypt(byte[] data,byte[] key){
		...
	}
}
class AESCrypter implements Crypter{
	byte[] encrypt(byte[] data,byte[] key){
		...
	}
	byte[] decrypt(byte[] data,byte[] key){
		...
	}
}
class DES3rypter implements Crypter{
	byte[] encrypt(byte[] data,byte[] key){
		...
	}
	byte[] decrypt(byte[] data,byte[] key){
		...
	}
}
```
例0中的接口，其实还是不够抽象。数据必须全部加载进内存才可以进行加解密。可以再抽象一下，比如
```
interface Crypter{
	/**密钥就没必要用流了，密钥不会很大，用流就属于过度设计了*/
	OutputStream encrypt(InputStream input,byte[] key);
	OutputStream decrypt(InputStream data,byte[] key);
}
```
## 提高可拓展性（降低耦合）的原则
* 单一职责原则（Single Responsibility Principle）；
* 开闭原则（Open Closed Principle）；
* 里氏替换原则（Liskov Substitution Principle）；
* 迪米特法则（Law of Demeter），又叫“最少知道法则”；
* 接口隔离原则（Interface Segregation Principle）；
* 依赖倒置原则（Dependence Inversion Principle）

外加两个

* 分层原则
  1. 基础逻辑+应用逻辑+集成逻辑
  2. 主体逻辑和具体细节分层
* 隔离原则
  纯函数与io操作隔离

例1
```
//根据本金、期数、日利率、还款方式计算还款计划。这是一段非常通用的代码，行业通用。
function generateRepayPlan(principal,term,dailyRate,repayWay){
	...//计算还款计划的核心代码
}
//根据借据id计算还款计划
function generateRepayPlan(iouId){
	const iou = queryIou(iouId)
	const dailyRate = loadConfig('/app-config.json').dailyRate
	return generateRepayPlan(iou.principal,iou.term,dailyRate,iou.repayWay)
}
```
例2
```
function generateRepayPlan(iouId){
	const iou = queryIou(iouId)
	const dailyRate = loadConfig('/app-config.json').dailyRate
	...//计算还款计划的核心代码
}
```
例1将逻辑分成了计算还款计划、获取函数参数两部分。然后组合它们。

例2将计算还款计划和获取函数参数耦合在一起。

如果有需求，需要优化计算还款计划算法的性能，对于例1，我们重写计算的逻辑即可。

对于例2，我们需要重写整段逻辑，即使其中获取配置的部分并没有发生变化。


例3

实现一个加解密的接口设计。

基础逻辑如下
```
interface Crypter{
	byte[] encrypt(byte[] data,byte[] key);//纯函数
	byte[] decrypt(byte[] data,byte[] key);
}
```
应用逻辑如下（根据具体应用场景特化接口，简化使用）
```
interface StringCrypter{
	String encrypt(String data,String key);//纯函数
	String decrypt(String data,String key);
}
```
集成逻辑如下
```
interface MomCrypter{
	String encrypt(String data);//秘钥呢？实现类中自己去读io获取
	String decrypt(String data);
}
```

函数式编程思想实现关注点分离，解除耦合的例子

例4
```
var list = List.of(1,2,3,4,5);
var transformed = list.stream().filter(it->it>3).map(it->it*2).toList();
```
filter的逻辑和map的逻辑分离了，但是实际执行只需要遍历一次集合。

简直完美（语法啰嗦）。不过java16中stream新增了一些方法，可以直接toList了。

如果需要拓展逻辑，我们可以返回一个stream，然后用户可以添加自己的过滤逻辑。

例5
```
var future = threadPool.submit(()->{
	...
})
try{
	var result = future.get()
}catch(Exception e){
	...
}
```
和例4差不多

例4中的stream，例5中的future，嗾使函数式编程里面monad。


## 关于java的动态性
* 类加载器（双亲委托机制）
* 动态生成类（javassist、asm、bytebuddy、javapoet）
* jdk动态代理（动态生成类，必须实现接口）
* cglib动态代理（动态生成字节码，支持继承实现类）
* java5的instrument api
* 各种设计模式的核心是多态，是利用jvm执行方法时的运行时绑定机制
* java的多态只支持单分派


思考：动态代理不能代理final类不能重写final方法，如何突破这个限制（不提倡，但是可以知道）？


## 对可拓展性设计的一些理解
1. 系统的架构属性，有些是互相冲突的！

    比如可拓展性和可读性。

    如果预留的拓展方向并没有发生拓展，那就亏大了，因为可读性是实实在在的被牺牲了。
2. 业务需求永远是发散的！

    很容易出现的情况是，预留的拓展方向为a、b、c，实际需求的发展方向是c、d、e。
3. 逻辑拆分比合并难十倍！

   一旦系统里用了某种设计，别人是不敢轻易改的。应用某种设计，一般就会合并某些通用的逻辑。
   后面再要分开，会十分困难。
   系统一开始没有设计，根据业务发展重构、优化，这样比较容易。

所以，设计的时候，应遵循的原则是先满足当前需求，然后考虑如果要支持拓展，是不是方便改造成可拓展的方式。
如果当前不支持拓展，但是很容易改造成可拓展方式，则留到以后有拓展需求的时候重构。
如果当前不支持拓展，以后改造成可拓展的方式比较难，那就再作权衡。