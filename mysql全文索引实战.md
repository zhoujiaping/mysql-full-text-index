1 创建表
```
CREATE TABLE articles (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
    title VARCHAR (200),
    body TEXT,
    description varchar(200),
    author varchar(200),
    FULLTEXT (title, body) WITH PARSER ngram
) ENGINE = INNODB DEFAULT CHARSET=utf8mb4 COMMENT='文章表';
```
2 造数据

执行 SqlDatabase

3 查询

SELECT * FROM articles WHERE MATCH (title,body) AGAINST ('Press' IN NATURAL LANGUAGE MODE);

SELECT * FROM articles WHERE MATCH (title,body) AGAINST ('+ssay' IN BOOLEAN MODE);

==> 18:00:58	SELECT * FROM articles WHERE MATCH (title,body) AGAINST ('+ssay' IN BOOLEAN MODE) LIMIT 0, 1000	1000 row(s) returned	4.688 sec / 0.453 sec

可以看到，查询耗时4.688秒

4 对比btree索引
alter table articles add index articles_idx_author(author);

select * from articles where author like 'Mer%';

==> 18:00:37	select * from articles where author like 'Mer%' LIMIT 0, 1000	1000 row(s) returned	0.047 sec / 0.094 sec

可以看到，查询耗时0.047秒

5 结论（可能不够全面）

磁盘空间：全文索引占用更多磁盘空间。

内存占用：？

查询性能：能用btree的时候，btree索引的性能优势非常大。

功能完备：btree索引只适用于后缀模糊，全文索引功能更强。
