import com.github.javafaker.Faker
import groovy.sql.Sql

/*
CREATE TABLE articles (
    id INT UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY,
    title VARCHAR (200),
    body TEXT,
    description varchar(200),
    author varchar(200),
    FULLTEXT (title, body) WITH PARSER ngram
) ENGINE = INNODB DEFAULT CHARSET=utf8mb4 COMMENT='文章表';
* */

class SqlDatabase {
    static void main(String[] args) {
        def sql = setUpDatabase()
        def faker = new Faker()
        (0..1000).each{
            batch->
                def book = faker.book()
            def data = (0..1000).collect{
                [title:book.title().toString().replaceAll(/'/,''),
                 description:book.publisher().toString().replaceAll(/'/,''),
                 body:book.genre().toString().replaceAll(/'/,''),
                 author:book.author().toString().replaceAll(/'/,''),
                ]
            }
            batchInsert(sql,data)
        }
    }
    def static batchInsert(Sql sql,List data){
        def values = data.collect{
            "('${it.title}','${it.description}','${it.body}','${it.author}')"
        }.join(",")
        def _sql = """
insert into articles(title,description,body,author)values $values;
"""
        println _sql
        sql.executeUpdate(_sql.toString())
    }

    static Sql setUpDatabase() {
        def url='jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8'
        def driver='com.mysql.cj.jdbc.Driver'
        def username='root'
        def passwd=''
        def sql = Sql.newInstance(url, username, passwd, driver)
        return sql
    }
}